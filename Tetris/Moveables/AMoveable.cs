﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Tetris
{
    public enum Input { up, down, left, right }
    abstract class AMoveable
    {
        private bool canBeDone = false;
        public Element[] elements = new Element[4];

        private delegate void IsOnBottom();
        private event IsOnBottom isOnBot;


        public bool isMoving { get; set; }

        public AMoveable()
        {
            isOnBot += Settings.gameMaster.GenerateNewMovingTetroid;
            isMoving = true;
        }
        public void Move(Input input)
        {
            switch (input)
            {
                case Input.down:
                    canBeDone = true;
                    try
                    {
                        foreach (var item in elements)
                        {
                            if (item.Row + 1 >= 20)
                                canBeDone = false;
                            /* if (Settings.takenFields.Contains(new Point<int, int>(item.Row + 1, item.Column)))
                            {
                                canBeDone = false;
                            }*/
                            foreach (var itema in Settings.takenFields)
                            {
                                if (item.Row + 1 == itema.Row && item.Column == itema.Column)
                                    canBeDone = false;
                            }
                        }
                    }

                    catch
                    {
                    }
                    finally
                    {
                        if (canBeDone)
                        {
                            foreach (var item in elements)
                            {
                                try
                                {
                                    item.Row++;
                                    item.setRow(item.form, item.Row);
                                }
                                catch
                                {
                                    break;
                                }
                            }
                        }
                        if (!canBeDone)
                        {
                            isMoving = false;
                            foreach (var item in elements)
                            {
                                Settings.takenFields.Add(new Point<int, int>(item.Row, item.Column));
                            }
                            Settings.staticTetrominoes.Add(this);
                            Settings.gameMaster.RemoveRow();
                            isOnBot();
                        }

                        canBeDone = false;
                    }
                    break;
                case Input.up:
                    Rotate();
                    break;
                case Input.left:
                    canBeDone = true;
                    try
                    {
                        foreach (var item in elements)
                        {
                            if (item.Column - 1 <= -1)
                                canBeDone = false;
                            if (Settings.takenFields.Contains(new Point<int, int>(item.Row, item.Column - 1)))
                            {
                                canBeDone = false;
                            }
                        }
                    }
                    catch
                    {

                    }
                    finally
                    {
                        if (canBeDone)
                        {
                            foreach (var item in elements)
                            {
                                try
                                {
                                    item.Column--;
                                    item.setColumn(item.form, item.Column);
                                }
                                catch
                                {
                                    break;
                                }
                            }
                            canBeDone = false;
                        }
                    }
                    break;
                case Input.right:
                    canBeDone = true;
                    try
                    {
                        foreach (var item in elements)
                        {
                            if (item.Column + 1 >= 14)
                                canBeDone = false;
                            if (Settings.takenFields.Contains(new Point<int, int>(item.Row, item.Column + 1)))
                            {
                                canBeDone = false;
                            }
                        }
                    }
                    catch
                    {

                    }
                    finally
                    {
                        if (canBeDone)
                        {
                            foreach (var item in elements)
                            {
                                try
                                {
                                    item.Column++;
                                    item.setColumn(item.form, item.Column);
                                }
                                catch
                                {
                                    break;
                                }
                            }
                            canBeDone = false;
                        }
                    }
                    break;

            }
        }
        protected abstract void Rotate();
    }
}
