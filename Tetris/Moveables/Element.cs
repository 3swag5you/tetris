﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Tetris
{
    class Element : INotifyPropertyChanged
    {
        public Rectangle form { get; set; }
        public bool isRootElement { get; set; }

        private int row;
        private int column;
        public int Row
        {
            get { return row; }

            set { if (value <= 19 || value == 21) { row = value; setRow(form, value); OnPropertyChanged(); } }
        }
        public int Column
        {
            get { return column; }

            set { if (value <= 13 && value >= 0) { column = value; setColumn(form, value); OnPropertyChanged(); } }
        }

        public Action<UIElement, int> setColumn = Grid.SetColumn;
        public Action<UIElement, int> setRow = Grid.SetRow;

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propname = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        public Element()
        {
            this.form = new Rectangle();
            form.Height = 30;
            form.Width = 30;
             
        }
    }
}
