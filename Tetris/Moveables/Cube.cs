﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Tetris.Moveables
{
    class Cube : AMoveable
    {
        public Cube()
        {
            elements[0] = new Element();
            elements[0].Column = 6;
            elements[0].Row = 0;
            elements[0].form.Fill = Brushes.Red;
            Settings.grid.Children.Add(elements[0].form);

            elements[1] = new Element();
            elements[1].Column = 7;
            elements[1].Row = 0;
            elements[1].form.Fill = Brushes.Red;
            Settings.grid.Children.Add(elements[1].form);

            elements[2] = new Element();
            elements[2].Column = 6;
            elements[2].Row = 1;
            elements[2].form.Fill = Brushes.Red;
            Settings.grid.Children.Add(elements[2].form);

            elements[3] = new Element();
            elements[3].Column = 7;
            elements[3].Row = 1;
            elements[3].form.Fill = Brushes.Red;
            Settings.grid.Children.Add(elements[3].form);

        }

        protected override void Rotate()
        {
            
        }
    }
}
