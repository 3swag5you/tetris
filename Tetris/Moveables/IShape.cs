﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Tetris.Moveables
{
    class IShape : AMoveable
    {
        private enum State { one, two }

        State state;
        public IShape()
        {

            elements[0] = new Element();
            elements[0].Column = 7;
            elements[0].Row = 0;
            elements[0].form.Fill = Brushes.Purple;
            Settings.grid.Children.Add(elements[0].form);

            elements[1] = new Element();
            elements[1].Column = 7;
            elements[1].Row = 1;
            elements[1].form.Fill = Brushes.Purple;
            Settings.grid.Children.Add(elements[1].form);

            elements[2] = new Element();
            elements[2].Column = 7;
            elements[2].Row = 2;
            elements[2].form.Fill = Brushes.Purple;
            Settings.grid.Children.Add(elements[2].form);

            elements[3] = new Element();
            elements[3].Column = 7;
            elements[3].Row = 3;
            elements[3].form.Fill = Brushes.Purple;
            Settings.grid.Children.Add(elements[3].form);

            state = State.one;
        }
        protected override void Rotate()
        {
            bool canSwitch = true;
            switch (state)
            {
                case State.one:
                    if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row, elements[1].Column - 1)))
                        canSwitch = false;
                    else if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row, elements[1].Column + 1)))
                        canSwitch = false;
                    else if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row, elements[1].Column + 2)))
                        canSwitch = false;
                    else if (elements[1].Column < 1)
                        canSwitch = false;
                    else if (elements[1].Column > 11)
                        canSwitch = false;
                    else
                    {
                        elements[0].Row = elements[1].Row;
                        elements[0].Column = elements[1].Column - 1;

                        elements[2].Row = elements[1].Row;
                        elements[2].Column = elements[1].Column + 1;

                        elements[3].Row = elements[1].Row;
                        elements[3].Column = elements[1].Column + 2;
                    }

                    state = State.two;
                    canSwitch = true;
                    break;
                case State.two:
                    if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row + 1, elements[1].Column)))
                        canSwitch = false;
                    else if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row - 1, elements[1].Column)))
                        canSwitch = false;
                    else if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Row - 2, elements[1].Column)))
                        canSwitch = false;
                    else if (elements[1].Row > 17)
                        canSwitch = false;
                    else
                    {
                        elements[0].Column = elements[1].Column;
                        elements[0].Row = elements[1].Row - 1;

                        elements[2].Row = elements[1].Row + 1;
                        elements[2].Column = elements[1].Column;

                        elements[3].Row = elements[1].Row + 2;
                        elements[3].Column = elements[1].Column;
                    }
                    state = State.one;
                    canSwitch = true;
                    break;
            }
        }
    }
}
