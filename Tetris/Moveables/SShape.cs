﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Tetris.Moveables
{
    class SShape : AMoveable
    {
        private enum State { first,second,third,fourth}
        State state = State.first;
        public SShape()
        {
            elements[0] = new Element();
            elements[0].Column = 6;
            elements[0].Row = 1;
            elements[0].form.Fill = Brushes.Yellow;
            Settings.grid.Children.Add(elements[0].form);

            elements[1] = new Element();
            elements[1].Column = 7;
            elements[1].Row = 1;
            elements[1].form.Fill = Brushes.Yellow;
            Settings.grid.Children.Add(elements[1].form);

            elements[2] = new Element();
            elements[2].Column = 7;
            elements[2].Row = 0;
            elements[2].form.Fill = Brushes.Yellow;
            Settings.grid.Children.Add(elements[2].form);

            elements[3] = new Element();
            elements[3].Column = 8;
            elements[3].Row = 0;
            elements[3].form.Fill = Brushes.Yellow;
            Settings.grid.Children.Add(elements[3].form);
        }

        protected override void Rotate()
        {
            switch (state)
            {
                case State.first:
                    if (RotateSecond())
                        break;
                    if (RotateThird())
                        break;
                    if (RotateFourth())
                        break;
                    break;
                case State.second:
                    if (RotateThird())
                        break;
                    if (RotateFourth())
                        break;
                    if (RotateFirst())
                        break;
                    break;
                case State.third:
                    if (RotateFourth())
                        break;
                    if (RotateFirst())
                        break;
                    if (RotateSecond())
                        break;
                    break;
                case State.fourth:
                    if (RotateFirst())
                        break;
                    if (RotateSecond())
                        break;
                    if (RotateThird())
                        break;
                    break;
            }
        }

        private bool RotateFirst()
        {
            bool canBeSwitched = true;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column + 1, elements[1].Row - 1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column, elements[1].Row -1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column-1, elements[1].Row)))
                canBeSwitched = false;
            if (elements[1].Column < 1)
                canBeSwitched = false;
            if (elements[1].Column > 12)
                canBeSwitched = false;

            if (canBeSwitched)
            {
                elements[3].Row = elements[1].Row - 1;
                elements[3].Column = elements[1].Column + 1;

                elements[2].Row = elements[1].Row -1;
                elements[2].Column = elements[1].Column;

                elements[0].Row = elements[1].Row;
                elements[0].Column = elements[1].Column -1;
            }

            state = State.first;
            return canBeSwitched;


        }
        private bool RotateSecond()
        {
            bool canBeSwitched = true;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column + 1, elements[1].Row + 1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column, elements[1].Row -1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column -1, elements[1].Row)))
                canBeSwitched = false;
            if (elements[1].Column < 1)
                canBeSwitched = false;
            if (elements[1].Column > 12)
                canBeSwitched = false;

            if (canBeSwitched)
            {
                elements[3].Row = elements[1].Row + 1;
                elements[3].Column = elements[1].Column + 1;

                elements[2].Row = elements[1].Row;
                elements[2].Column = elements[1].Column + 1;

                elements[0].Row = elements[1].Row - 1;
                elements[0].Column = elements[1].Column;
            }

            state = State.second;
            return canBeSwitched;

        }
        private bool RotateThird()
        {
            bool canBeSwitched = true;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column - 1, elements[1].Row + 1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column, elements[1].Row + 1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column +1, elements[1].Row)))
                canBeSwitched = false;
            if (elements[1].Column < 1)
                canBeSwitched = false;
            if (elements[1].Column > 12)
                canBeSwitched = false;

            if (canBeSwitched)
            {
                elements[3].Row = elements[1].Row + 1;
                elements[3].Column = elements[1].Column - 1;

                elements[2].Row = elements[1].Row + 1;
                elements[2].Column = elements[1].Column;

                elements[0].Row = elements[1].Row;
                elements[0].Column = elements[1].Column +1;
            }
            state = State.third;
            return canBeSwitched;

        }
        private bool RotateFourth()
        {
            bool canBeSwitched = true;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column - 1, elements[1].Row - 1)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column -1, elements[1].Row)))
                canBeSwitched = false;
            if (Settings.takenFields.Contains(new Point<int, int>(elements[1].Column, elements[1].Row +1)))
                canBeSwitched = false;
            if (elements[1].Column < 1)
                canBeSwitched = false;
            if (elements[1].Column > 12)
                canBeSwitched = false;

            if (canBeSwitched)
            {
                elements[3].Row = elements[1].Row - 1;
                elements[3].Column = elements[1].Column - 1;

                elements[2].Row = elements[1].Row;
                elements[2].Column = elements[1].Column - 1;

                elements[0].Row = elements[1].Row + 1;
                elements[0].Column = elements[1].Column;
            }
            state = State.fourth;
            return canBeSwitched;

        }
    }
}
