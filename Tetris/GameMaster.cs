﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Tetris
{
    class GameMaster
    {
        public AMoveable moving;
        DispatcherTimer dt = new DispatcherTimer();
        public void Init()
        {
            dt.Interval = new TimeSpan(0, 0, 0, 0, 500);
            dt.Start();
            dt.Tick += MoveDown;
        }

        public void HandleInput(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                moving.Move(Input.down);
            }
            if (e.Key == Key.Up)
            {
                moving.Move(Input.up);
            }
            if (e.Key == Key.Left)
            {
                moving.Move(Input.left);
            }
            if (e.Key == Key.Right)
            {
                moving.Move(Input.right);
            }
        }
        private void MoveDown(object sender, EventArgs e)
        {
            moving.Move(Input.down);
        }

        public void GenerateNewMovingTetroid()
        {

            moving = GenerateNewMoveable.generate();
        }

        public void RemoveRow()
        {

            int row1, row2, row3, row4, row5, row6, row7, row8, row9, row10, row11, row12, row13, row14, row15, row16, row17, row18, row19;
            row1 = row2 = row3 = row4 = row5 = row6 = row7 = row8 = row9 = row10 = row11 = row12 = row13 = row14 = row15 = row16 = row17 = row18 = row19 = 0;
            foreach (var moveable in Settings.staticTetrominoes)
            {
                foreach (var element in moveable.elements)
                {
                    switch (element.Row)
                    {
                        case 1:
                            ++row1;
                            break;
                        case 2:
                            ++row2;
                            break;
                        case 3:
                            ++row3;
                            break;
                        case 4:
                            ++row4;
                            break;
                        case 5:
                            ++row5;
                            break;
                        case 6:
                            ++row6;
                            break;
                        case 7:
                            ++row7;
                            break;
                        case 8:
                            ++row8;
                            break;
                        case 9:
                            ++row9;
                            break;
                        case 10:
                            ++row10;
                            break;
                        case 11:
                            ++row11;
                            break;
                        case 12:
                            ++row12;
                            break;
                        case 13:
                            ++row13;
                            break;
                        case 14:
                            ++row14;
                            break;
                        case 15:
                            ++row15;
                            break;
                        case 16:
                            ++row16;
                            break;
                        case 17:
                            ++row17;
                            break;
                        case 18:
                            ++row18;
                            break;
                        case 19:
                            ++row19;
                            break;

                    }
                }
            }

            if (row1 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 1)
                            element.Row = 21;

                        if (element.Row < 1)
                            ++element.Row;
                    }
                    row1 = 0;
                    UpdateTakenFields(1);
                }
            }
            if (row2 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 2)
                            element.Row = 21;

                        if (element.Row < 2)
                            ++element.Row;
                    }
                }
                row2 = 0;
                UpdateTakenFields(2);
            }

            if (row3 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 3)
                            element.Row = 21;

                        if (element.Row < 3)
                            ++element.Row;
                    }
                }
                row3 = 0;
                UpdateTakenFields(3);
            }
            if (row4 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 4)
                            element.Row = 21;

                        if (element.Row < 4)
                            ++element.Row;
                    }
                }
                row4 = 0;
                UpdateTakenFields(4);
            }
            if (row5 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 5)
                            element.Row = 21;

                        if (element.Row < 5)
                            ++element.Row;
                    }
                }
                row5 = 0;
                UpdateTakenFields(5);
            }
            if (row6 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 6)
                            element.Row = 21;

                        if (element.Row < 6)
                            ++element.Row;
                    }
                }
                row6 = 0;
                UpdateTakenFields(6);
            }
            if (row7 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 7)
                            element.Row = 21;

                        if (element.Row < 7)
                            ++element.Row;
                    }
                }
                row7 = 0;
                UpdateTakenFields(7);
            }
            if (row8 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 8)
                            element.Row = 21;

                        if (element.Row < 8)
                            ++element.Row;
                    }
                }
                row8 = 0;
                UpdateTakenFields(8);
            }
            if (row9 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 9)
                            element.Row = 21;

                        if (element.Row < 9)
                            ++element.Row;
                    }
                }
                row9 = 0;
                UpdateTakenFields(9);
            }
            if (row10 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 10)
                            element.Row = 21;

                        if (element.Row < 10)
                            ++element.Row;
                    }
                }
                row10 = 0;
                UpdateTakenFields(10);
            }
            if (row11 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 11)
                            element.Row = 21;

                        if (element.Row < 11)
                            ++element.Row;
                    }
                }
                row11 = 0;
                UpdateTakenFields(11);
            }
            if (row12 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 12)
                            element.Row = 21;

                        if (element.Row < 12)
                            ++element.Row;
                    }
                }
                row12 = 0;
                UpdateTakenFields(12);
            }
            if (row13 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 13)
                            element.Row = 21;

                        if (element.Row < 13)
                            ++element.Row;
                    }
                }
                row13 = 0;
                UpdateTakenFields(13);
            }
            if (row14 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 14)
                            element.Row = 21;

                        if (element.Row < 14)
                            ++element.Row;
                    }
                }
                row14 = 0;
                UpdateTakenFields(14);
            }
            if (row15 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 15)
                            element.Row = 21;

                        if (element.Row < 15)
                            ++element.Row;
                    }
                }
                row15 = 0;
                UpdateTakenFields(15);
            }
            if (row16 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 16)
                            element.Row = 21;

                        if (element.Row < 16)
                            ++element.Row;
                    }
                }
                row16 = 0;
                UpdateTakenFields(16);
            }
            if (row17 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 17)
                            element.Row = 21;

                        if (element.Row < 17)
                            ++element.Row;
                    }
                }
                row17 = 0;
                UpdateTakenFields(17);
            }
            if (row18 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 18)
                            element.Row = 21;

                        if (element.Row < 18)
                            ++element.Row;
                    }
                }
                row18 = 0;
                UpdateTakenFields(18);
            }
            if (row19 == 14)
            {
                foreach (var item in Settings.staticTetrominoes)
                {
                    foreach (var element in item.elements)
                    {
                        if (element.Row == 19)
                            element.Row = 21;

                        if (element.Row < 19)
                            ++element.Row;
                    }
                }
                UpdateTakenFields(19);
                row19 = 0;
            }
            row1 = row2 = row3 = row4 = row5 = row6 = row7 = row8 = row9 = row10 = row11 = row12 = row13 = row14 = row15 = row16 = row17 = row18 = row19 = 0;
        }

        private void UpdateTakenFields(int row)
        {
            foreach (var item in Settings.takenFields)
            {
                if (item.Row <= row)
                    item.Row++;
            }
        }

    }
}
