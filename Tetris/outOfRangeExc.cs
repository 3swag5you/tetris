﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class outOfRangeExc : Exception
    {
        public outOfRangeExc() : base()
        {

        }

        public outOfRangeExc(String message) : base(message)
        {

        }
    }
}
