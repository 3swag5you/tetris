﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Tetris
{
    class Point<T,Z>
    {
        public T Row { get; set; }
        public Z Column { get; set; }

        public Point(T t, Z z)
        {
            this.Row = t;
            this.Column = z;
        }
    }
    class Settings
    {
       public static  Grid grid;
        public static GameMaster gameMaster;
        public static List<Point<int,int>> takenFields = new List<Point<int, int>>();
        public static List<AMoveable> staticTetrominoes = new List<AMoveable>();
    }
}
