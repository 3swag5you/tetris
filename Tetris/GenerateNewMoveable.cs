﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tetris.Moveables;

namespace Tetris
{
    class GenerateNewMoveable
    {
        public static AMoveable generate()
        {
            Random randy = new Random();
            int x = randy.Next(0, 7);
            switch(x)
            {
                case 0:
                    return new Cube();
                case 1:
                    return new IShape();
                case 2:
                    return new LShape();
                case 3:
                    return new ReverseLShape();
                case 4:
                    return new SShape();
                case 5:
                    return new TShape();
                case 6:
                    return new ZShape();
            }
            return new Cube();
        }
    }
}
